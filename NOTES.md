# Notes

- https://github.com/desktop/desktop/blob/development/docs/integrations/gitlab.md
- https://github.com/joaopalmeiro/prettier-vue-config
- https://github.com/withastro/prettier-plugin-astro?tab=readme-ov-file#recommended-configuration
- https://github.com/prettier/prettier/releases
- https://github.com/withastro/prettier-plugin-astro:
  - https://github.com/withastro/prettier-plugin-astro/blob/v0.13.0/package.json
  - https://github.com/withastro/prettier-plugin-astro/releases
- https://github.com/will-stone/prettier-config:
  - https://github.com/will-stone/prettier-config/blob/v7.0.3/index.json
- Prettier configuration:
  - https://github.com/withastro/astro/blob/astro%404.6.0/prettier.config.js
  - https://github.com/withastro/starlight/blob/%40astrojs/starlight-tailwind%402.0.2/.prettierrc
  - https://github.com/withastro/prettier-plugin-astro/blob/v0.13.0/.prettierrc.json
  - https://github.com/withastro/storefront/blob/aa8217f1c02cdde1488c5cb94665469af569869d/prettier.config.js
- https://nodejs.org/api/esm.html#importmetaresolvespecifier:
  - "v20.0.0 This API now returns a string synchronously instead of a Promise."
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/import.meta/resolve
- https://github.com/prettier/prettier/issues/8474#issuecomment-1536605670:
  - "(...) have you tried `require('your-prettier-plugin')` instead of `'your-prettier-plugin'` in `.prettierrc`? ([example](https://github.com/kachkaev/routine-npm-packages/blob/89653e236797256bf4b85a671c2c27dd178775c3/packages/prettier-config/main.cjs)). Without `require` (or `import` in Prettier v3), plugins may fail to load in certain package managers."
- https://github.com/prettier/prettier/issues/14905#issuecomment-1577304501:
  - "Plugin autoloading in pnpm and Yarn PnP has been an issue for quite some time: [#8474](https://github.com/prettier/prettier/issues/8474)"
  - "We have removed plugin search feature completely ([#14759](https://github.com/prettier/prettier/pull/14759)), so please define your plugin [in the config file](https://prettier.io/docs/en/configuration.html) (e.g. `prettier.config.cjs`): `{ plugins: [require("@prettier/plugin-ruby")] }`"
- https://github.com/pnpm/pnpm/issues/4700
- https://github.com/tailwindlabs/prettier-plugin-tailwindcss/issues/207#issuecomment-1698071122
- https://github.com/threlte/threlte/pull/281/files
- https://github.com/threlte/threlte/blob/%40threlte/core%407.3.0/.prettierrc
- https://github.com/pnpm/pnpm/issues/4700#issuecomment-1598422677
- https://prettier.io/docs/en/plugins:
  - "Strings provided to `plugins` are ultimately passed to `import() expression`, so you can provide a module/package name, a path, or anything else `import()` takes."
- https://github.com/prettier/prettier/issues/15667:
  - "It's hard to find out where to import that plugin. I suggest use: `url.pathToFileURL(require.resolve('prettier-plugin-tailwindcss'))` or `import.meta.resolve('prettier-plugin-tailwindcss')`"
  - https://github.com/prettier/prettier/issues/15667#issuecomment-1828437371
  - https://github.com/prettier/prettier/issues/15667#issuecomment-1849291338: "Using `import.meta.resolve(...)` solves the issue."
  - https://github.com/prettier/prettier/issues/15085#issuecomment-1636993346
- https://github.com/prettier/prettier/issues/15667#issuecomment-1833357273:
  - "(...) I guess your plugin is installed in `node_modules/` instead of `node_modules/@kachkaev/prettier-config/node_modules/`, so Prettier can import from your `.prettierc`, but it's possible to be in other places with different package managers, so it can be broken."
  - "Hmmm yeah possibly. It must have been hoisted by pnpm which has made it available. Surprisingly, it works both in CLI and VS Code."
- https://github.com/prettier/prettier-vscode/issues/3066:
  - "Changing `plugins: ['plugin-name']` to `plugins: [require.resolve('plugin-name')]` works for me in both vscode and precommit"
- [requireConfig only if there's prettier config, and then also use .editorconfig](https://github.com/prettier/prettier-vscode/issues/2402) (open) issue:
  - https://github.com/prettier/prettier-vscode/issues/2402
  - https://github.com/prettier/prettier-vscode/pull/2708
  - Currently, `prettier.requireConfig` is not doing what is expected because an `.editorconfig` file exists in this project.
- https://stackoverflow.com/questions/72672786/vscode-json-schemas-run-validator-on-command-line
- https://code.visualstudio.com/docs/languages/json#_json-schemas-and-settings
- https://code.visualstudio.com/docs/languages/json#_mapping-to-a-schema-in-the-workspace
- https://prettier.io/docs/en/configuration.html#configuration-schema
- https://www.schemastore.org/json/
- https://semver.npmjs.com/
- https://github.com/prettier/prettier/blob/main/CHANGELOG.md#325
- https://prettier.io/blog/2024/06/01/3.3.0.html
- https://github.com/bluwy/publint/releases

## Commands

```bash
rm -rf node_modules/ && npm install
```
